package com.ds2.dao;

import com.ds2.pojo.User;
import com.ds2.utils.MybatisUtil;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: mybatis-study
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-03-22
 **/
public class UserTest {


    @Test
    public void getUserList() {
        SqlSession sqlSession = MybatisUtil.getSqlSession();
        User2Mapper mapper = sqlSession.getMapper(User2Mapper.class);
        mapper.getUserList().forEach(
            u -> System.out.println(u)
        );
        sqlSession.close();
    }

    @Test
    public void getByid() {
        SqlSession sqlSession = MybatisUtil.getSqlSession();
        User2Mapper mapper = sqlSession.getMapper(User2Mapper.class);
        User byId = mapper.getById(1);
        System.out.println(byId);
        sqlSession.close();
    }
    @Test
    public void addUSer(){
        SqlSession sqlSession = MybatisUtil.getSqlSession();
        User2Mapper mapper = sqlSession.getMapper(User2Mapper.class);
        User user = new User();
        user.setName("呜呜呜");
        user.setPwd("1234124");
        int i = mapper.addUser(user);
        System.out.println(i);
        System.out.println("插入之后的ID是"+user.getId());
        sqlSession.commit();
        sqlSession.close();
    }
    @Test
    public void updateUser(){
        SqlSession sqlSession = MybatisUtil.getSqlSession();
        User2Mapper mapper = sqlSession.getMapper(User2Mapper.class);
        User user = new User(8, "heihei", "123");
        int i = mapper.updateUser(user);
        System.out.println(i);
        sqlSession.commit();
        sqlSession.close();
    }
    @Test
    public void delUser(){
        SqlSession sqlSession = MybatisUtil.getSqlSession();
        User2Mapper mapper = sqlSession.getMapper(User2Mapper.class);
        int i = mapper.deleteUser(8);
        System.out.println(i);
        sqlSession.commit();
        sqlSession.close();
    }
    @Test
    public void addUserMap(){
        SqlSession sqlSession = MybatisUtil.getSqlSession();
        User2Mapper mapper = sqlSession.getMapper(User2Mapper.class);
        Map<String, Object> hashMap = new HashMap<>();
        hashMap.put("userName","map集合");
        hashMap.put("userPwd","4123");
        int i = mapper.addUserMap(hashMap);
        System.out.println(i);
        sqlSession.commit();
        sqlSession.close();
    }
    @Test
    public void getByList(){
        SqlSession sqlSession = MybatisUtil.getSqlSession();
        User2Mapper mapper = sqlSession.getMapper(User2Mapper.class);
        Map<String, Object> hashMap = new HashMap<>();
        hashMap.put("userName","张");
        List<User> like = mapper.getLike(hashMap);
        like.forEach(u->System.out.println(u));
    }
}
