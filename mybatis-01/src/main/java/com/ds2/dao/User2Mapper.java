package com.ds2.dao;

import com.ds2.pojo.User;

import java.util.List;
import java.util.Map;

/**
 * @program: mybatis-study
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-03-22
 **/
public interface User2Mapper {
    List<User> getUserList();

    User getById(int id);

    int addUser(User user);

    int updateUser(User user);

    int deleteUser(int id);

    int addUserMap(Map<String,Object> msp);

    List<User> getLike(Map<String,Object> map);
}
