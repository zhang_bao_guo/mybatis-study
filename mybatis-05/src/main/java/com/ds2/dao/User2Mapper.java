package com.ds2.dao;

import com.ds2.pojo.User;
import org.apache.ibatis.annotations.Select;

/**
 * @program: mybatis-study
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-03-22
 **/
public interface User2Mapper {
    @Select("select id,name,pwd as password from USER where id=#{id}")
    User getById(int id);
}
