package com.ds2.dao;

import com.ds2.pojo.User;
import com.ds2.util.MybatisUtil;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: mybatis-study
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-03-22
 **/
public class UserTest {

    @Test
    public void getByid() {
        SqlSession sqlSession = MybatisUtil.getSqlSession();
        User2Mapper mapper = sqlSession.getMapper(User2Mapper.class);
        User byId = mapper.getById(1);
        System.out.println(byId);
        sqlSession.close();
    }
}
