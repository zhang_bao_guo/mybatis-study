package com.ds2.dao;

import com.ds2.pojo.Blog;
import com.ds2.util.IDUtils;
import com.ds2.util.MybatisUtil;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @program: mybatis-study
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-03-30
 **/
public class blogTest {
    @Test
    public void T1(){
        SqlSession sqlSession = MybatisUtil.getSqlSession();
        BlogMapper mapper = sqlSession.getMapper(BlogMapper.class);
        Blog blog = new Blog();
        blog.setId(IDUtils.getID());
        blog.setTitle("tttt");
        blog.setAuthor("张三");
        blog.setViews(412);
        int i = mapper.addBlog(blog);
        System.out.println(i);
        System.out.println(blog.getId());
        sqlSession.close();
    }
    @Test
    public void T2(){
        SqlSession sqlSession = MybatisUtil.getSqlSession();
        BlogMapper mapper = sqlSession.getMapper(BlogMapper.class);
        List<Blog> blogList = new ArrayList<Blog>();
        for (int i = 0; i < 20 ; i++) {
            Blog blog = new Blog();
            blog.setId(IDUtils.getID());
            blog.setTitle("tttt"+i);
            blog.setAuthor("张三"+i);
            blog.setViews(412);
            blogList.add(blog);
        }
        mapper.addBlogList(blogList);
        sqlSession.commit();
        sqlSession.close();
    }
    @Test
    public void T3(){
        SqlSession sqlSession = MybatisUtil.getSqlSession();
        BlogMapper mapper = sqlSession.getMapper(BlogMapper.class);
        Blog blog = new Blog();
        blog.setTitle("如此简单");
        blog.setViews(100);
        List<Blog> byQuery = mapper.getByQuery(blog);
        for (Blog blog1 : byQuery) {
            System.out.println(blog1);
        }
        sqlSession.close();
    }
    @Test
    public void T4(){
        SqlSession sqlSession = MybatisUtil.getSqlSession();
        BlogMapper mapper = sqlSession.getMapper(BlogMapper.class);
        Blog blog = new Blog();
        blog.setId("4");
        blog.setTitle("cloud如此简单");
        blog.setAuthor("张三丰");
        mapper.updateBlog(blog);
        sqlSession.commit();
        sqlSession.close();
    }

    @Test
    public void T5chacheLV1(){
        SqlSession sqlSession = MybatisUtil.getSqlSession();
        BlogMapper mapper = sqlSession.getMapper(BlogMapper.class);
        Blog blog = new Blog();
        blog.setTitle("如此简单");
        blog.setViews(100);
        List<Blog> byQuery = mapper.getByQuery(blog);
        for (Blog blog1 : byQuery) {
            System.out.println(blog1);
        }
        System.out.println("===============");
        List<Blog> byQuery1 = mapper.getByQuery(blog);
        for (Blog blog1 : byQuery1) {
            System.out.println(blog1);
        }

        sqlSession.close();
    }

    @Test
    public void T5chacheLV2() {
        SqlSession sqlSession = MybatisUtil.getSqlSession();
        BlogMapper mapper = sqlSession.getMapper(BlogMapper.class);

        Blog blog = new Blog();
        blog.setTitle("如此简单");
        blog.setViews(100);
        List<Blog> byQuery = mapper.getByQuery(blog);
        for (Blog blog1 : byQuery) {
            System.out.println(blog1);
        }
        sqlSession.close();

        SqlSession sqlSession1 = MybatisUtil.getSqlSession();
        BlogMapper mapper1 = sqlSession1.getMapper(BlogMapper.class);
        Blog blog1 = new Blog();
        blog1.setTitle("如此简单");
        blog1.setViews(100);
        List<Blog> byQuery1 = mapper1.getByQuery(blog1);
        for (Blog blog2 : byQuery1) {
            System.out.println(blog2);
        }
        sqlSession1.close();
    }
}
