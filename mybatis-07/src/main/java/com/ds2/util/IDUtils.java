package com.ds2.util;

import org.junit.Test;

import java.util.UUID;

/**
 * @program: mybatis-study
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-03-30
 **/
public class IDUtils {
    public static String getID(){
        UUID uuid = UUID.randomUUID();
        String s = uuid.toString();
        String replace = s.replace("-", "");
        return replace;
    }
}
