package com.ds2.dao;

import com.ds2.pojo.Blog;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @program: mybatis-study
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-03-30
 **/
public interface BlogMapper {
    int addBlog(Blog blog);
    int addBlogList(@Param("blogList") List<Blog> blogList);
    List<Blog> getByQuery(Blog blog);
    int updateBlog(Blog blog);
}
