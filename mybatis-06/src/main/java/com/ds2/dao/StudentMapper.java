package com.ds2.dao;

import com.ds2.pojo.Student;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @program: mybatis-study
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-03-28
 **/
public interface StudentMapper {
    List<Student> getStuList();
}
