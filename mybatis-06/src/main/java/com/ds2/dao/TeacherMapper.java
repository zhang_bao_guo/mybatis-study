package com.ds2.dao;

import com.ds2.pojo.Teacher;

/**
 * @program: mybatis-study
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-03-28
 **/
public interface TeacherMapper {
    Teacher getTeacher();
}
