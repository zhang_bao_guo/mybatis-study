package com.ds2.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @program: mybatis-study
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-03-28
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Teacher {
    private int tid;
    private String tname;
    private List<Student> stu;
}
