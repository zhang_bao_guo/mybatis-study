package com.ds.dao;

import com.ds2.dao.User2Mapper;
import com.ds2.pojo.User;
import com.ds2.util.MybatisUtil;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.junit.Test;


/**
 * @program: mybatis-study
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-03-22
 **/
public class UserTest {
   static Logger logger = Logger.getLogger(UserTest.class);
    @Test
    public void getByid() {
        SqlSession sqlSession = MybatisUtil.getSqlSession();
        User2Mapper mapper = sqlSession.getMapper(User2Mapper.class);
//        logger.info("info:查询开始");
        User byId = mapper.getById(1);
        User user = new User();
        System.out.println(byId);
        sqlSession.close();
//        logger.debug("info:查询结束");
    }
    @Test
    public void log4JTest(){

    }
}
