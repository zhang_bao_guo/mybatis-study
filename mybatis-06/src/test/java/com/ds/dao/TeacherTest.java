package com.ds.dao;

import com.ds2.dao.TeacherMapper;
import com.ds2.pojo.Student;
import com.ds2.util.MybatisUtil;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.List;

/**
 * @program: mybatis-study
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-03-28
 **/
public class TeacherTest {

    @Test
    public void T1(){
        SqlSession sqlSession = MybatisUtil.getSqlSession();
        TeacherMapper mapper = sqlSession.getMapper(TeacherMapper.class);
        System.out.println(mapper.getTeacher());
        List<Student> stu = mapper.getTeacher().getStu();
        for (Student student : stu) {
            System.out.println(student);
        }
    }
}
