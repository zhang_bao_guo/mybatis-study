package com.ds.dao;

import com.ds2.dao.StudentMapper;
import com.ds2.pojo.Student;
import com.ds2.util.MybatisUtil;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.List;

/**
 * @program: mybatis-study
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-03-28
 **/
public class StudentTest {
    @Test
    public void Test1(){
        SqlSession sqlSession = MybatisUtil.getSqlSession();
        StudentMapper mapper = sqlSession.getMapper(StudentMapper.class);
        List<Student> stuList = mapper.getStuList();
        for (Student student : stuList) {
            System.out.println(student);
        }
    }
}
