package com.ds2.dao;

import com.ds2.pojo.User;
import com.ds2.util.MybatisUtil;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @program: mybatis-study
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-03-22
 **/
public class UserTest {
   static Logger logger = Logger.getLogger(UserTest.class);
    @Test
    public void getByid() {
        SqlSession sqlSession = MybatisUtil.getSqlSession();
        User2Mapper mapper = sqlSession.getMapper(User2Mapper.class);
        logger.info("info:查询开始");
        User byId = mapper.getById(1);
        System.out.println(byId);
        sqlSession.close();
        logger.debug("info:查询结束");
    }
    @Test
    public void log4JTest(){

    }

    @Test
    public void getUserByPage(){
        SqlSession sqlSession = MybatisUtil.getSqlSession();
        logger.info("查询开始-------------");
        Map<String,Object> map = new HashMap<String, Object>();
        map.put("pageNum",1);
        map.put("pageSize",3);
        User2Mapper mapper = sqlSession.getMapper(User2Mapper.class);
        List<User> userByPage = mapper.getUserByPage(map);
        for (User user : userByPage) {
            System.out.println(user);
        }
        logger.info("查询结束-------------");
        sqlSession.close();
    }

    @Test
    public void getAll(){
        SqlSession sqlSession = MybatisUtil.getSqlSession();
        User2Mapper mapper = sqlSession.getMapper(User2Mapper.class);
        List<User> byPageHelp = mapper.getByPageHelp();
        for (User user : byPageHelp) {
            System.out.println(user);
        }
    }
}
