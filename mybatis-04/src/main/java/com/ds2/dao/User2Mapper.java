package com.ds2.dao;

import com.ds2.pojo.User;

import java.util.List;
import java.util.Map;

/**
 * @program: mybatis-study
 * @description:
 * @author: Dawson.Zhang
 * @create: 2020-03-22
 **/
public interface User2Mapper {
    User getById(int id);

    List<User> getUserByPage(Map<String,Object> map);

    List<User> getByPageHelp();
}
